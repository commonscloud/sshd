#!/bin/bash

set -e;

# Universal user
SSH_USER=user

echo "Create User $SSH_USER"

# Clean old users
if getent passwd $SSH_USER_UID >/dev/null 2>&1; then
   echo "User exist!"
   echo "Clean old user"
   sed -i "/:$SSH_USER_UID:/d" /etc/passwd
   sed -i "/:$SSH_USER_UID:/d" /etc/group
fi

# Define shell
if [[ -z "${SSH_SHELL}" ]]; then
	echo "SSH login shell not present"
	SSH_SHELL=/sbin/nologin
fi

# Create user
adduser -D -g $SSH_USER $SSH_USER -u $SSH_USER_UID -h /home/user -s $SSH_SHELL

# Define password
if [[ -z "${SSH_PASS}" ]]; then
	echo "SSH not present"
	echo "Generate random password"
	SSH_PASS=`pwgen 64 1`
fi

# Change password
echo "$SSH_USER:$SSH_PASS" | chpasswd

# Clean pass
export SSH_PASS=secret

echo "Start SSH"
exec $@
