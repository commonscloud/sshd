FROM alpine:latest

RUN apk update && \
	apk add openssh-server \
		vim nano rsync pwgen mysql-client sed git bash

RUN /usr/bin/ssh-keygen -A

COPY ./sshd.conf /etc/ssh/sshd_config.d/sshd.conf

COPY ./motd /etc/motd

ADD ./entrypoint.sh /entrypoint.sh
ADD ./start.sh /start.sh

RUN chmod 500 /entrypoint.sh /start.sh

VOLUME ["/etc/ssh"]

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/start.sh"]
