# Simple SSH server

Simple SSH server using alpine image.

## Configure Docker Compose

services:
  ssh:
    image: registry.commonscloud.coop/sftp
    build: .
    ports:
      - 0.0.0.0:2222:22
    volumes:
      - .data:/home/user/data
    environment:
      - SSH_USER_UID=1000
      - SSH_AUTH_KEY=<ssh_pub_key>
    restart: always

## Access

 $ ssh 127.0.0.1 -luser -p 2222 -i </path/ssh_key
